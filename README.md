


# 1º Semestre - GlaDOS
<div align="center">
    <img src="./Tutorial/glados.png" width="320" />
    <div height="50"></div>
</div>
Com a proposta do Projeto Integrador de fornecer uma solução para que os alunos buscassem um problema, foi desenvolvido o projeto GlaDOS.
A solução apresentada foi a construção de um WEB Bot que deveria atender uma demanda específica de um usuário final.<br>
Pensando na proposta, foi analisada e levantada a ideia de construção de um procurador de ofertas na Amazon, que tinha como objetivo buscar os melhores preços dentro da plataforma e calcular sua porcentagem de desconto quando houvesse.

## Objetivos
Para suprir necessidades de radar de preços para pessoas físicas, GlaDOS tinha os seguintes objetivos:
- Busca de preços em sites de compra
Para que o usuário tivesse uma listagem sem precisar acessar o site evetivamente, o sistema trará todos os itens encontrados pelo campo de busca com seus respectivos preços.
- Análise de desconto
Objetivo central do projeto GlaDOS, quando identificado que um determinado produto estava em promoção, um cálculo era realizado para definir a porcentagem de desconto sobre o mesmo.
- Comparativo de preços
Para assegurar a compra do usuário, é realizado um comparativo de preços em outros sites de compras para verificar qual a melhor opção no momento.

## Tecnologias
* Python <br>
Para atender a proposta de buscar informações, foi discutido e definida a utilização da linguagem de programação Python visando a utilização de duas principais bibliotecas **requests** e **lxml**.
O uso em conjunto das bibliotecas mencionadas permitiam a principal função do projeto, a raspagem de dados (Web Scraping), que possui a finalidade de buscar informações precisas dentro de HTLMs de sites, além de ser possível o preenchimento em campos de texto.
*  Vue.js <br>
Também foi definido que seria utilizada uma interface gráfica para mostrar os resultados da busca pelo produto. Para isso foi utilizado o FrameWork do JavaScript **Vue,js**, que permitia a conexão de um Back-end em Python com uma interface gráfica, também fazendo uso do sistema SPA(Single Page Application) para uma melhor utilização pelo usuário final.
* MySQL <br>
Para o armazenamento das informações coletadas nos sites, foi utilizado o Sistema de Gerenciamento de Banco de Dados (SGBD), sendo escolhido como indicações de professores no desenvolvimento do projeto.

## Contribuições individuais
### Contribuição na implementação
No desenvolvimento da GlaDOS, fui designado para atuar de forma mais ativa no desenvolvimento do Back-end, desta forma, desenvolvi o código central do projeto, a raspagem de dados.
O primeiro objetivo dentro do projeto foi a realização da consulta a partir de uma busca de um produto qualquer.
> Código utilizado para buscar um produto a partir de um input
```python
 ##linhas para inserção de um produto a ser pesquisado
	busca = input("Digite o nome do produto: ")
	link = ('https://www.amazon.com.br/s?k=ProdutoPesquisado&__mk_pt_BR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&ref=nb_sb_noss')
##método definido para substituir o parâmetro de pesquisa na Amazon.
	url = link.replace("ProdutoPesquisado",busca.strip())
##conexão com o link do produto pesquisado
	res = requests.get(url, headers={'user-agent': 'glados'})
##para verificar se a página já estava pronta para realizar a consulta
##foi feito o loop abaixo checando se o código é 503 onde a página ainda esta sendo carregada
	while(res.status_code == 503):
	 print('Sem conexão! :c')
```
Com o código acima é possível acessar a página de pesquisa da Amazon e partir para o acesso as informações dos produtos conforme o código.
> Raspagem de informações referentes aos produtos pesquisados
```python
##inserindo todo o conteúdo de uma página na variável doc
	doc = html.fromstring(res.content)
##a partir da doc, é possível acessar todos os componentes do HTML através de um
##identificador, neste caso buscamos pelo "id" de um elemento dentro deste documento
##que contém o nome de determinado produto
	nome_produto = doc.xpath('.//*[@id="productTitle"]/text()')
##categoria do produto
	categoria = doc.xpath('.//*[@id="wayfindingbreadcrumbs_feature_div"]/ul/li[1]/span/a/text()')
	cat = separador.join(categoria).strip()
##preço cheio do produto
	fullprice = doc.xpath('.//*[@id="buyBoxInner"]/ul/li[1]/span/span[2]/text()')
	fullprice2 = doc.xpath('.//*[@id="price"]/table/tbody/tr[1]/td[2]/span[1]/text()')
##disponibilidade do produto
	av = doc.xpath('.//*[@id="availability"]/span/text()')
```
Com os códigos acima, é possível acessar e capturar informações nas páginas da Amazon, efetuando assim o processe de Web Scraping.

### Contribuição na documentação
Considerando o nível de maturidade profissional de cada integrante do time, foi feita uma iniciativa de documentação de tutorial para entendimento de código e execução do aplicativo.
Foi montado um tutorial de criação e manipulação de [Banco de Dados](https://gitlab.com/omnitron/glados/-/blob/master/Tutorial/Criar%20Banco%20SQL.pdf), com este guia foi possível nivelar os conhecimentos gerais da equipe, permitindo que todos possuíssem um conhecimento introdutório ao banco de dados.
Conforme o projeto evoluía, dificuldades na criação do código e utilização da biblioteca foram levantadas e com isso foi criado um guia para [Web Scraping](https://gitlab.com/omnitron/glados/-/blob/master/Tutorial/tutorial.pdf) com a utilização das bibliotecas **lxml** e **request**.

## Aprendizados efetivos
* Utilização de ferramentas de controle de versão de arquivos(Git); <br>
 O contato inicial com ferramenta permitiu uma melhor gestão dos arquivos que envolviam o desenvolvimento do projeto, a utilização da ferramenta foi tão bem vista que perdurou pelos demais semestres para controle de entregas e segurança no compartilhamento do projeto.
* Utilização da Metodologia Ágil (SCRUM); <br>
Com a ideia de acompanhar e identificar gargalos durante a produção do projeto, a ideia de Daily Meeting foi introduzida para acompanhar o desenvolvimento, assim foi possível compreender a importância de acompanhar um projeto de maneira mais efetiva com ferramentas de Sprint, Planning e Reviews.
* Aprendizado de desenvolvimento em Python <br>
O primeiro contato com linguagem de programação, inicialmente um desafio que foi se tornando uma experiência mais amigável. Com o Python foi possível entender conceitos de lógica de programação que foram levados para os próximos semestres.
* Conexão Python com a base de dados utilizada; <br>
Além da utilização da linguagem Python em si, foi estudado e aprendido a conexão com o Banco de Dados para o armazenamento de dados para o Projeto. Antes este armazenamento era realizado em arquivos de texto mas com a migração para o banco de dados, foi observado o aumento no desempenho e organização de dados no projeto.
* Concepção do Mínimo Produto Viável; <br>
O conceito chave para todas as primeiras entregas de projetos a partir do primeiro API. Neste ensinamento foi entendido a importância de priorizar os entregáveis para o cliente.
